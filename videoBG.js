
var videoMaterial = document.getElementById("myVideo");

$(".btn").click(function() {
  if(videoMaterial.paused) {
    videoMaterial.play();
    $("#play").removeClass("bg");
    $("#pause").addClass("bg");
  } else {
    videoMaterial.pause();
    $("#play").addClass("bg");
    $("#pause").removeClass("bg");
  }
});
